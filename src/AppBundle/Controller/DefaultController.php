<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

use AppBundle\Model\Fixture;
use AppBundle\Model\MatchReport;

/**
 * Class DefaultController
 * @package AppBundle\Controller
 */
class DefaultController extends Controller
{
    /**
     * This is just for debugging purposes
     * Json feed will be converted to PHP object [ready to manipulate, e.g. save in db etc. ]
     * @Route("/")
     */
    public function indexAction()
    {
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);

        // lets assume this json came from feed
        $fixtureJson = '[{"createdAt":"2016-08-15 10:10:10","teams":[{"name":"Manchester United","players":[{"name":"Obama"},{"name":"Boris"}]},{"name":"Manchester City","players":[{"name":"Putin"},{"name":"Trump"}]}],"kickoffDatetime":"2016-08-16 15:00:00","result":"0-1","location":"Manchester"}]';
        $fixture = json_decode($fixtureJson, true);

        $objArray = [];
        foreach ($fixture as $player) {
            $jsonContent = json_encode($player);
            /** @var Fixture $reportObj */
            $playerObj = $serializer->deserialize($jsonContent, 'AppBundle\Model\Fixture', 'json');
            $objArray[] = $playerObj;
        }
        print('<pre>');
        print_r($objArray);
        print('</pre>');

        // lets assume this json came from feed
        $matchReportJson = '[{"createdAt":"2016-08-15 10:10:10","teams":[{"name":"Manchester United","players":[{"name":"Obama"},{"name":"Boris"}]},{"name":"Manchester City","players":[{"name":"Putin"},{"name":"Trump"}]}],"kickoffDatetime":"2016-08-16 15:00:00","location":"Manchester","players":[{"name":"Putin"},{"name":"Trump"},{"name":"Obama"}],"playersScoreGoal":[{"name":"Putin"},{"name":"Trump"}],"scoresTime":["2016-08-16 15:30:00","2016-08-16 15:31:00"],"playersWithCard":[{"name":"Putin"},{"name":"Trump"}],"timesCardGiven":["2016-08-16 15:40:10","2016-08-16 15:45:08"]}]';
        $matchReport = json_decode($matchReportJson, true);

        $objArray1 = [];
        foreach ($matchReport as $report) {
            $jsonContent = json_encode($report);
            /** @var MatchReport $reportObj */
            $reportObj = $serializer->deserialize($jsonContent, 'AppBundle\Model\MatchReport', 'json');
            // if there is score in report, trigger SMS
            if ($reportObj->getScoresTime()) {
                // send SMS
            }

            $objArray1[] = $reportObj;
        }
        print('<pre>');
        print_r($objArray1);
        print('</pre>');
        // if you cannot see result here, just see the twig file

        return $this->render('AppBundle:Default:index.html.twig', []);
    }

}
