<?php

namespace AppBundle\Model;

/**
 * Class Fixture
 * @package AppBundle\Model
 */
class Fixture extends BaseFixture
{
    protected $matchReport;

    /**
     * @return mixed
     */
    public function getMatchReport()
    {
        return $this->matchReport;
    }

    /**
     * @param mixed $matchReport
     */
    public function setMatchReport($matchReport)
    {
        $this->matchReport = $matchReport;
    }
}