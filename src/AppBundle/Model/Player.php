<?php

namespace AppBundle\Model;

/**
 * Class Player
 * @package AppBundle\Model
 */
class Player
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
}