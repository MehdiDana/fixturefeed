<?php

namespace AppBundle\Model;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class Team
 * @package AppBundle\Model
 */
class Team
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var array Player
     */
    protected $players;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return array
     */
    public function getPlayers()
    {
        return $this->players;
    }

    /**
     * @param array $players
     */
    public function setPlayers($players)
    {
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);

        $objArray = [];
        foreach ($players as $player) {
            $jsonContent = json_encode($player);
            $playerObj = $serializer->deserialize($jsonContent, 'AppBundle\Model\Player', 'json');
            $objArray[] = $playerObj;
        }
        $this->players = $objArray;
    }


}