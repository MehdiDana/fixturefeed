<?php

namespace AppBundle\Model;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class MatchReport
 * @package AppBundle\Model
 */
class MatchReport extends BaseFixture
{
    /**
     * @var
     */
    protected $players;

    /**
     * @var
     */
    protected $playersScoreGoal;

    /**
     * @var
     */
    protected $scoresTime;

    /**
     * @var
     */
    protected $playersWithCard;

    /**
     * @var
     */
    protected $timesCardGiven;

    /**
     * @return mixed
     */
    public function getPlayers()
    {
        return $this->players;
    }

    /**
     * @param mixed $players
     */
    public function setPlayers($players)
    {
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);

        $objArray = [];
        foreach ($players as $player) {
            $jsonContent = json_encode($player);
            $playerObj = $serializer->deserialize($jsonContent, 'AppBundle\Model\Player', 'json');
            $objArray[] = $playerObj;
        }
        $this->players = $objArray;
    }

    /**
     * @return mixed
     */
    public function getPlayersScoreGoal()
    {
        return $this->playersScoreGoal;
    }

    /**
     * @param mixed $playersScoreGoal
     */
    public function setPlayersScoreGoal($playersScoreGoal)
    {
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);

        $objArray = [];
        foreach ($playersScoreGoal as $player) {
            $jsonContent = json_encode($player);
            $playerObj = $serializer->deserialize($jsonContent, 'AppBundle\Model\Player', 'json');
            $objArray[] = $playerObj;
        }
        $this->playersScoreGoal = $objArray;
    }

    /**
     * @return mixed
     */
    public function getScoresTime()
    {
        return $this->scoresTime;
    }

    /**
     * @param mixed $scoresTime
     */
    public function setScoresTime($scoresTime)
    {
        $this->scoresTime = $scoresTime;
    }

    /**
     * @return mixed
     */
    public function getPlayersWithCard()
    {
        return $this->playersWithCard;
    }

    /**
     * @param mixed $playersWithCard
     */
    public function setPlayersWithCard($playersWithCard)
    {
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);

        $objArray = [];
        foreach ($playersWithCard as $player) {
            $jsonContent = json_encode($player);
            $playerObj = $serializer->deserialize($jsonContent, 'AppBundle\Model\Player', 'json');
            $objArray[] = $playerObj;
        }
        $this->playersWithCard = $objArray;
    }

    /**
     * @return mixed
     */
    public function getTimesCardGiven()
    {
        return $this->timesCardGiven;
    }

    /**
     * @param mixed $timesCardGiven
     */
    public function setTimesCardGiven($timesCardGiven)
    {
        $this->timesCardGiven = $timesCardGiven;
    }

}