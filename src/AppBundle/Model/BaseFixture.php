<?php

namespace AppBundle\Model;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class BaseFixture
 * @package AppBundle\Model
 */
class BaseFixture
{
    /**
     * @var
     */
    protected $createdAt;

    /**
     * @var
     */
    protected $teams;

    /**
     * @var
     */
    protected $kickoffDatetime;

    /**
     * @var
     */
    protected $result;

    /**
     * @var
     */
    protected $location;

    /**
     * BaseFixture constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getTeams()
    {
        return $this->teams;
    }

    /**
     * @param array $teams
     */
    public function setTeams($teams)
    {
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);

        $objArray = [];
        foreach ($teams as $team) {
            $jsonContent = json_encode($team);
            $teamObj = $serializer->deserialize($jsonContent, 'AppBundle\Model\Team', 'json');
            $objArray[] = $teamObj;
        }
        $this->teams = $objArray;
    }

    /**
     * @return mixed
     */
    public function getKickoffDatetime()
    {
        return $this->kickoffDatetime;
    }

    /**
     * @param mixed $kickoffDatetime
     */
    public function setKickoffDatetime($kickoffDatetime)
    {
        $this->kickoffDatetime = $kickoffDatetime;
    }

    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param mixed $result
     */
    public function setResult($result)
    {
        $this->result = $result;
    }

    /**
     * @return mixed
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param mixed $location
     */
    public function setLocation($location)
    {
        $this->location = $location;
    }

}